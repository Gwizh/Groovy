<< Aujourd'hui nous savons ce que sont les étoiles, nous savons que la Terre tourne autour du Soleil, nous savons éclairer nos nuits avec nos propres lumières. >>

*Description du docteur et de la secte*
Le Docteur Rouage n'était pas vraiment un docteur dans le sens actuel du terme. Il se faisait appeler comme tel car il avait un savoir 

<<  >>

*Description du jardin et première apparition dans le récit des mosaïques*
C'était un luxuriant jardin entre le marbre du temple. Le cimetière sacré était non loin et le tout était baigné d'une lumière douce, une nappe blanche et tiède. Les arbres créaient des zones d'ombres inégales mais immobiles, il n'y avait ni vent ni nuages. Des petites fontaines rafraichissaient l'air, lançant des petites gouttes d'eau dans toutes les directions. Les fleurs actaient comme le marbre blanc en réfléchissant cette eau plutôt que la lumière. Il y avait ça et là des petits cloches accrochées aux arbres pour ajouter à l'ambiance déjà dispercée une sorte de brumisateur de son. Et c'est pourquoi dans la rude chaleur estivale, ce lieu était un oasis de calme, de plénitude. On ne devait pas parler, ce lieu était reservé au silence. Entouré de grands murs, on entendait à peine l'agitation de la capitale. 

*Description de Val Rouage* 
**Une fille hyperactive qui est douée dans de nombreux domaines sans jamais suivre la norme. Elle s'ennuie dans un monde qui ne veut pas d'elle mais elle aime sa famille. Elle décore comme elle peut son monde qu'elle veut changer de l'intérieur.**
Depuis sa naissance, Val Rouage avait été très permétique à toutes ces idées. Troisième enfant du docteur, elle était la seule fille et avait toujours eu des difficultés à l'école. Elle était extrèmement dissipée, hyperactive, mais curieuse. Il était très difficile pour elle de rester en salle de classe et de consacrer son esprit à une seule tache. 

*Suite du mystère*
**Le docteur découvre que c'est sa fille qui est à l'origine de tout ça. Il est furieux qu'on critique sa pensée**




*Résumé*
